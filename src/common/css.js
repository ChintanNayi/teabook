import { StyleSheet } from 'react-native';

//dashboard page css 
const dashboardStyles = StyleSheet.create({
    fview: {
        flexDirection: 'row',
        left: 100,
        top: 80
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        shadowRadius: 10,
        borderRadius: 15,
        backgroundColor: '#EEF0F5',
        width: 300,
        height: 170,
        left: 60,
        top: 280
    },
    img: {
        width: '20%',
        height: 100,
        top: 0
    },
    headtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 30,
        top: 20,
        left: 10,
    },
    text: {
        fontWeight: 'bold',
        color: '#C58E4C',
        top: 10,
        paddingLeft: 10
    },
    neomorph: {
        shadowRadius: 50,
        borderRadius: 15,
        elevation: 5,
        backgroundColor: '#EEF0F5',
        height: 'auto',
        margin: 12,
        top: 90,
    },
    btnNopm: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 100,
        height: 30,
        right: 10,
        top: 20,
        alignItems: 'center'
    },
    btnListNopm: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 100,
        height: 30,
        left: 5,
        top: 20,
        alignItems: 'center'
    },
    btnAddNopm: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 100,
        height: 30,
        left: 5,
        top: 35,
        alignItems: 'center'
    },
    modalView: {
        alignItems: "center",
        elevation: 5,
        marginBottom: 35
    },
    closeText: {
        fontSize: 24,
        color: '#00479e',
        textAlign: 'center',
    },
    addtxtnmop: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 200,
        height: 50,
        top: 20
    },
    tinput: {
        borderBottomColor: '#EEF0F5'
    },

});

//add Company page css 
const addCompanyStyles = StyleSheet.create({
    fview: {
        flexDirection: 'row',
        left: 100,
        top: 10
    },
    headtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 30,
        top: 20,
        left: 10,
    },
    img: {
        width: '20%',
        height: 100,
        top: 0
    },
    sview: {
        height: '100%',
        top: 10,
        flexDirection: 'column',
        flex: 1
    },
    tinput: {
        borderBottomColor: '#EEF0F5'
    },
    txtnmop: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 375,
        height: 50,
        alignItems: 'center',
        margin: 10
    },
    addtxtnmop: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 375,
        height: 150,
        alignItems: 'center',
        margin: 10
    },
    btnNopm: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 200,
        height: 50,
        left: 110,
        top: 10,
        alignItems: 'center'
    }

});

//Login Screen page css 
const loginStyles = StyleSheet.create({
    fview: {
        flexDirection: 'row',
        left: 100,
        top: 80
    },
    headtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 30,
        top: 20,
        left: 10,
    },
    img: {
        width: '20%',
        height: 100,
        top: 0
    },
    sview: {
        height: '100%',
        top: 70,
        flexDirection: 'column',
        flex: 1
    },
    tinput: {
        borderBottomColor: '#EEF0F5',
        alignSelf: 'center'
    }

});

//SignUp page css 
const signupStyles = StyleSheet.create({
    fview: {
        flexDirection: 'row',
        left: 100,
        top: 10
    },
    headtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 30,
        top: 20,
        left: 10,
    },
    img: {
        width: '20%',
        height: 100,
        top: 0
    },
    sview: {
        height: '100%',
        top: 10,
        flexDirection: 'column',
        flex: 1
    },
    tinput: {
        borderBottomColor: '#EEF0F5'
    },
    txtnmop: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 375,
        height: 50,
        margin: 10,
        alignItems: 'center'
    },
    addtxtnmop: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 375,
        height: 150,
        margin: 10,
        alignItems: 'center'
    },
    btnNopm: {
        shadowRadius: 10,
        borderRadius: 25,
        backgroundColor: '#EEF0F5',
        width: 200,
        height: 50,
        left: 110,
        top: 10,
        alignItems: 'center'
    }

});

const ModelStyles = StyleSheet.create({
    modelView: {
        alignItems: "center",
        elevation: 5,
        marginBottom: 35,
    },
    modelCentered: {
        flex: 1,
        shadowRadius: 3,
        borderRadius: 100,
        backgroundColor: '#EEF0F5',
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 280,
    },
});

export { dashboardStyles, addCompanyStyles, loginStyles, signupStyles, ModelStyles };