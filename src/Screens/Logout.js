import React, { Component } from 'react';
import { View, Alert, Text, Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Shadow, Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eff: false
        }
        this.showAlert();
    }

    showAlert(){
        Alert.alert(
            'Logout',
            'Are you sure? You want to logout?',
            [
              {
                text: 'Cancel',
                onPress: () => {
                  return this.props.navigation.navigate('Dashboard');
                },
              },
              {
                text: 'Confirm',
                onPress: () => {
                  AsyncStorage.clear();
                  this.props.navigation.replace('LoginScreens');
                },
              },
            ],
            {cancelable: false},
          )
    }

    render() {
        return (
            <View>

            </View>
        )
    }
}
