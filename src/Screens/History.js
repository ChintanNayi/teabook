import React, { Component } from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image, ScrollView } from 'react-native';
import { Neomorph } from 'react-native-neomorph-shadows';
import TeaCup from '../Assets/tea_logo.png';
import data from '../common/data';

class History extends Component {
    constructor(props) {
        super(props)

        this.state = {
            eff: false,
            show: false,
            tableHead: [
                {
                    name:'Date'
                },
                {
                    name:'Time      '
                },
                {
                    name:'Tea Count'
                }
            ],
            widthArr: [100, 100, 100],

            payment: 'done'
        }
        
    }

    render() {
        const state = this.state;
        return (
            <SafeAreaView style={{ backgroundColor: '#EEF0F5', height: '100%', flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                    <View style={styles.fview}>
                        <Image style={styles.img} source={TeaCup} />
                        <Text style={styles.headtext}>Payment History</Text>
                    </View>
                    <View style={styles.sview}>
                        <Neomorph
                            inner // <- enable shadow inside of neomorph
                            swapShadows // <- change zIndex of each shadow color
                            style={{
                                shadowRadius: 10,
                                borderRadius: 15,
                                backgroundColor: '#EEF0F5',
                                width: 375,
                                height: 510,
                                top: 30,
                                left: 10,
                                alignItems: 'center'
                            }}
                        >
                            <View>
                                <Text style={styles.tableHeadtext}>{this.props.route.params.cName}</Text>
                            </View>
                            <View style={{ height: 450, marginTop: 20 }}>
                                <View style={{ flexDirection: 'row', width: 300,justifyContent:'space-between' }}>
                                    {
                                        state.tableHead.map((rowData, index) => (
                                            <Text style={{textAlign:'center',fontWeight:'bold',color:'#C58E4C'}} key={index}>{rowData.name}</Text>
                                        ))
                                    }

                                </View>
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    {
                                        data.map((data, i) => (
                                            <View key={i} style={{ flexDirection: 'row',paddingBottom:5,paddingTop:5,color:'grey' }}>
                                                <Text style={{ marginRight: 20,color:'grey' }}>{data.date}</Text>
                                                <Text style={{ marginLeft: 10,color:'grey' }}>{data.time}</Text>
                                                <Text style={{ marginLeft: 40,color:'grey' }}>{data.count}</Text>
                                            </View>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </Neomorph>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    fview: {
        flexDirection: 'row',
        left: 50,
        top: 20
    },
    img: {
        width: '20%',
        height: 100,
        top: 0
    },
    headtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 25,
        top: 20,
        left: 10,
    },
    text: {
        fontWeight: 'bold',
        color: '#C58E4C',
        right: 20,
        top: 12
    },
    neomorph: {
        shadowRadius: 50,
        borderRadius: 15,
        elevation: 5,
        backgroundColor: '#EEF0F5',
        width: 320,
        height: 'auto',
        margin: 12,
        top: 20,
    },
    tableHeadtext: {
        textAlign: 'center',
        color: '#C58E4C',
        fontWeight: 'bold',
        fontSize: 20,
        top: 10,
    },
});

export default History;