import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert, Image, StyleSheet } from 'react-native';
import { Shadow, Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import MainImg from '../Assets/tea_main.png';
import TeaCup from '../Assets/tea_logo.png';
import AsyncStorage from '@react-native-community/async-storage';

export default class WelcomeScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {

    }
  }

  componentDidMount() {

    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      AsyncStorage.getItem('isLogin').then((value) =>
      this.props.navigation.replace(
          value === null ? 'LoginScreens' : 'tab'
        ),
      );
      // this.props.navigation.navigate('Login');
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <View style={{ backgroundColor: '#EEF0F5', height: '100%' }}>
        <Image style={{ top: 15, left: 125, height: '20%', width: '40%' }} source={TeaCup} />
        <Image style={styles.img} source={MainImg} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headtext: {
    textAlign: 'center',
    color: '#C58E4C',
    fontWeight: 'bold',
    fontSize: 25,
    top: 30
  },
  img: {
    width: '100%',
    height: '60%',
    top: 50,
  },
});
