import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Modal, Image, Button } from 'react-native';
import { Neomorph } from 'react-native-neomorph-shadows';
import { Input } from 'react-native-elements';
import TeaCup from '../Assets/tea_logo.png';
import AsyncStorage from '@react-native-community/async-storage';
import { loginStyles, ModelStyles } from '../common/css';
import * as Progress from 'react-native-progress';


export default class LoginScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      eff: false,
      isVisible: false,
    }

  }

  Login = () => {
    this.setState({ eff: true,isVisible:true })
    AsyncStorage.setItem('isLogin', 'login');
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      this.setState({ isVisible: false, eff: false })
      this.props.navigation.navigate('tab')
    }, 1000);

  }

  render() {
    return (
      <View style={{ backgroundColor: '#EEF0F5', height: '100%' }}>
        <View style={loginStyles.fview}>
          <Image style={loginStyles.img} source={TeaCup} />
          <Text style={loginStyles.headtext}>Login</Text>
        </View>

        <View style={loginStyles.sview}>

          <Neomorph
            inner // <- enable shadow inside of neomorph
            swapShadows // <- change zIndex of each shadow color
            style={{
              shadowRadius: 10,
              borderRadius: 25,
              backgroundColor: '#EEF0F5',
              width: 360,
              height: 50,
              left: 25,
              marginTop: 50
            }}
          >
            <Input
              placeholder="Enter User Name"
              inputContainerStyle={loginStyles.tinput}
              onChangeText={value => this.setState({ comment: value })}
            />
          </Neomorph>
          <Neomorph
            inner // <- enable shadow inside of neomorph
            swapShadows // <- change zIndex of each shadow color
            style={{
              shadowRadius: 10,
              borderRadius: 25,
              backgroundColor: '#EEF0F5',
              width: 360,
              height: 50,
              left: 25,
              top: 20
            }}
          >
            <Input
              secureTextEntry={true}
              placeholder="Enter Password"
              inputContainerStyle={loginStyles.tinput}
              onChangeText={value => this.setState({ comment: value })}
            />
          </Neomorph>
          <TouchableOpacity onPress={() => this.Login()}>
            <Neomorph

              inner={this.state.eff} // <- enable shadow inside of neomorph
              swapShadows // <- change zIndex of each shadow color
              style={{
                shadowRadius: 10,
                borderRadius: 25,
                backgroundColor: '#EEF0F5',
                width: 200,
                height: 50,
                left: 110,
                top: 100,
                alignItems: 'center'
              }}
            >
              <Text style={{ paddingTop: 15, fontWeight: 'bold', color: '#C58E4C', }}>Login</Text>
            </Neomorph>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('Signup') }}>
            <Text style={{ top: 120, left: 180, fontWeight: 'bold' }}>Register!</Text>
          </TouchableOpacity>
        </View>

        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.isVisible}
          onRequestClose={() => console.log("close")}>
          <View style={ModelStyles.modelView}>
            <Neomorph
              inner
              style={ModelStyles.modelCentered}
            >
              <Progress.Circle borderWidth={15} style={{ top: 20 }} color='#C58E4C' size={80} indeterminate={true}>
                <Neomorph
                  style={{
                    shadowRadius: 7,
                    borderRadius: 50,
                    backgroundColor: '#EEF0F5',
                    width: 40,
                    height: 40,
                    bottom: 60,
                    left: 20
                  }}
                />
              </Progress.Circle>
            </Neomorph>
            </View>
        </Modal>

      </View>

    )
  }
}
