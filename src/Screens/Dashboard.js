import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, ScrollView, TouchableOpacity, Modal } from 'react-native';
import { Neomorph } from 'react-native-neomorph-shadows';
import TeaCup from '../Assets/tea_logo.png';
import * as Progress from 'react-native-progress';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import { Input } from 'react-native-elements';
import { dashboardStyles, ModelStyles } from '../common/css'

export default class Dashboard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            show: false,
            list: [
                {
                    name: 'Vibrant Softtech',
                },
                {
                    name: 'Tata Motors',
                },
                {
                    name: 'Reliance Industries',
                }
            ],
            index: '',
            isVisible: false,
            cName: '',
            eff: false,
            hIn: false,
            isLodding: false,
        }
    }

    show = (i, n) => {
        this.setState({
            show: !this.state.show,
            index: i,
            cName: n
        })
    }

    // hide show modal
    displayModal(show) {
        this.setState({ eff: true })
        this.timeoutHandle = setTimeout(() => {
            this.setState({
                eff: false,
                isVisible: show
            })
        }, 100);
    }

    handleHistory(name) {
        this.setState({ hIn: true, isLodding: true })
        this.timeoutHandle = setTimeout(() => {
            this.setState({ hIn: false, isLodding: false })
            this.props.navigation.navigate('History', { cName: name });
        }, 1000);
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#EEF0F5', height: '100%', flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                    <View style={dashboardStyles.fview}>
                        <Image style={dashboardStyles.img} source={TeaCup} />
                        <Text style={dashboardStyles.headtext}>Dashboard</Text>
                    </View>
                    {
                        this.state.list.map((l, i) => (
                            <View key={i}>
                                <View style={dashboardStyles.neomorph}>
                                    <TouchableOpacity onPress={() => this.show(i, l.name)}>
                                        <View style={{ flexDirection: 'row', height: 45 }}>
                                            <Icon1 name="office-building" color='grey' style={{ paddingLeft: 15, paddingTop: 8 }} size={25} />
                                            <Text style={dashboardStyles.text}>{l.name}</Text>
                                            {/* <Icon1 name="office-building" color='grey' style={{ paddingRight: 15, paddingTop: 8 }} size={25} /> */}
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {this.state.index === i ?
                                    <Neomorph
                                        inner
                                        swapShadows // <- change zIndex of each shadow color
                                        style={{
                                            shadowRadius: 10,
                                            borderRadius: 15,
                                            backgroundColor: '#EEF0F5',
                                            width: 370,
                                            height: 130,
                                            top: 90,
                                            left: 15,
                                        }}
                                    >
                                        <View>
                                            <View style={{ flexDirection: 'column', paddingLeft: 35, paddingTop: 20, alignItems: 'flex-start' }}>
                                                <View style={{ flexDirection: 'column' }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', right: 10 }}>Date:</Text>
                                                        <Text style={{ right: 5, }}>18/01/2021</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', marginLeft: 10 }}>Time:</Text>
                                                        <Text style={{ paddingLeft: 5, }}>10:30AM</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', marginLeft: 15 }}>Tea Count:</Text>
                                                        <Text style={{ paddingLeft: 5, }}>9</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', right: 10 }}>Date:</Text>
                                                        <Text style={{ right: 5, }}>18/01/2021</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', marginLeft: 10 }}>Time:</Text>
                                                        <Text style={{ paddingLeft: 5, }}>04:30PM</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold', marginLeft: 15 }}>Tea Count:</Text>
                                                        <Text style={{ paddingLeft: 5, }}>9</Text>
                                                    </View>

                                                    <View style={{ flexDirection: 'row' }}>
                                                        <TouchableOpacity onPress={() => {
                                                            this.displayModal(true);
                                                        }}>
                                                            <Neomorph
                                                                inner={this.state.eff}
                                                                swapShadows // <- change zIndex of each shadow color
                                                                style={dashboardStyles.btnNopm}>
                                                                <Text style={{ paddingTop: 5, fontWeight: 'bold', color: '#C58E4C', }}>Add Tea</Text>
                                                            </Neomorph>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => { this.handleHistory(l.name) }}>
                                                            <Neomorph
                                                                inner={this.state.hIn}
                                                                swapShadows // <- change zIndex of each shadow color
                                                                style={dashboardStyles.btnListNopm}>
                                                                <Text style={{ paddingTop: 5, fontWeight: 'bold', color: '#C58E4C', }}>History</Text>
                                                            </Neomorph>
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>

                                            </View>
                                        </View>
                                    </Neomorph>
                                    : null}

                            </View>
                        ))
                    }
                </ScrollView>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => console.log("close")}>
                    <Neomorph
                        // inner
                        swapShadows // <- change zIndex of each shadow color
                        style={dashboardStyles.centeredView}
                    >

                        <View style={dashboardStyles.modalView}>
                            <Text style={{ fontWeight: 'bold', color: '#C58E4C' }}>{this.state.cName}</Text>
                            <Text style={{ fontWeight: 'bold', top: 5, color: '#C58E4C' }}>18/01/2021</Text>
                            <Neomorph
                                inner // <- enable shadow inside of neomorph
                                swapShadows // <- change zIndex of each shadow color
                                style={dashboardStyles.addtxtnmop}>
                                <Input
                                    placeholder="Enter Tea"
                                    inputContainerStyle={dashboardStyles.tinput}
                                    keyboardType="numeric"
                                    onChangeText={value => this.setState({ comment: value })}
                                />
                            </Neomorph>
                            <TouchableOpacity
                                onPress={() => {
                                    this.displayModal(!this.state.isVisible);
                                }}
                            >
                                <Neomorph
                                    inner={this.state.eff}
                                    swapShadows // <- change zIndex of each shadow color
                                    style={dashboardStyles.btnAddNopm}>
                                    <Text style={{ paddingTop: 5, fontWeight: 'bold', color: '#C58E4C', }}>Add Tea</Text>
                                </Neomorph>
                            </TouchableOpacity>

                        </View>
                    </Neomorph>
                </Modal>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isLodding}
                    onRequestClose={() => console.log("close")}>
                    <View style={ModelStyles.modelView}>
                        <Neomorph
                            inner
                            style={ModelStyles.modelCentered}
                        >
                            <Progress.Circle borderWidth={15} style={{ top: 20 }} color='#C58E4C' size={80} indeterminate={true}>
                                <Neomorph
                                    style={{
                                        shadowRadius: 7,
                                        borderRadius: 50,
                                        backgroundColor: '#EEF0F5',
                                        width: 40,
                                        height: 40,
                                        bottom: 60,
                                        left: 20
                                    }}
                                />
                            </Progress.Circle>
                        </Neomorph>
                    </View>
                </Modal>
            </SafeAreaView>
        )
    }
}

