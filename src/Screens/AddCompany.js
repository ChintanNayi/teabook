import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, SafeAreaView } from 'react-native';
import { Shadow, Neomorph, NeomorphBlur } from 'react-native-neomorph-shadows';
import { Input, Icon } from 'react-native-elements';
import TeaCup from '../Assets/tea_logo.png';
import { addCompanyStyles } from '../common/css';

export default class AddCompany extends Component {
    constructor(props) {
        super(props)

        this.state = {
            eff: false
        }
    }
    render() {
        return (

            <SafeAreaView style={{ backgroundColor: '#EEF0F5', height: '100%', flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                    <View style={addCompanyStyles.fview}>
                        <Image style={addCompanyStyles.img} source={TeaCup} />
                        <Text style={addCompanyStyles.headtext}>New Office</Text>
                    </View>

                    <View style={addCompanyStyles.sview}>
                        <Neomorph
                            inner // <- enable shadow inside of neomorph
                            swapShadows // <- change zIndex of each shadow color
                            style={addCompanyStyles.txtnmop}>
                            <Input
                                placeholder="Office Name"
                                inputContainerStyle={addCompanyStyles.tinput}
                                onChangeText={value => this.setState({ comment: value })}
                            />
                        </Neomorph>

                        <Neomorph
                            inner // <- enable shadow inside of neomorph
                            swapShadows // <- change zIndex of each shadow color
                            style={addCompanyStyles.txtnmop}>
                            <Input
                                placeholder="Mobile Number"
                                inputContainerStyle={addCompanyStyles.tinput}
                                onChangeText={value => this.setState({ comment: value })}
                            />
                        </Neomorph>
                        <Neomorph
                            inner // <- enable shadow inside of neomorph
                            swapShadows // <- change zIndex of each shadow color
                            style={addCompanyStyles.addtxtnmop}>
                            <Input
                                placeholder="Office Address"
                                numberOfLines={3}
                                multiline={true}
                                inputContainerStyle={addCompanyStyles.tinput}
                                onChangeText={value => this.setState({ comment: value })}
                            />
                        </Neomorph>
                        <Neomorph
                            inner={this.state.eff} // <- enable shadow inside of neomorph
                            swapShadows // <- change zIndex of each shadow color
                            style={addCompanyStyles.btnNopm}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Dashboard') }}>
                                <Text style={{ paddingTop: 15, fontWeight: 'bold', color: '#C58E4C', }}>Add New Office</Text>
                            </TouchableOpacity>
                        </Neomorph>
                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }
}


