import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image,ScrollView,SafeAreaView } from 'react-native';
import {  Neomorph } from 'react-native-neomorph-shadows';
import { Input } from 'react-native-elements';
import TeaCup from '../Assets/tea_logo.png';
import { signupStyles } from '../common/css';


export default class SignupScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
            eff: false
        }
    }
    render() {
        return (
           
            <SafeAreaView style={{ backgroundColor: '#EEF0F5', height: '100%',flex:1 }}>
               <ScrollView contentContainerStyle={{ flexGrow: 1}} showsVerticalScrollIndicator={false}>
                <View style={signupStyles.fview}>
                    <Image style={signupStyles.img} source={TeaCup} />
                    <Text style={signupStyles.headtext}>Register</Text>
                </View>
               
                <View style={signupStyles.sview}>
                    <Neomorph
                        inner // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.txtnmop}>
                        <Input
                            placeholder="User Name"
                            inputContainerStyle={signupStyles.tinput}
                            onChangeText={value => this.setState({ comment: value })}
                        />
                    </Neomorph>
                    <Neomorph
                        inner // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.txtnmop}>
                        <Input
                            secureTextEntry={true}
                            placeholder="Password"
                            inputContainerStyle={signupStyles.tinput}
                            onChangeText={value => this.setState({ comment: value })}
                        />
                    </Neomorph>
                    <Neomorph
                        inner // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.txtnmop}>
                        <Input
                            secureTextEntry={true}
                            placeholder="Confirm Password"
                            inputContainerStyle={signupStyles.tinput}
                            onChangeText={value => this.setState({ comment: value })}
                        />
                    </Neomorph>
                    <Neomorph
                        inner // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.txtnmop}>
                        <Input
                            placeholder="Mobile Number"
                            inputContainerStyle={signupStyles.tinput}
                            onChangeText={value => this.setState({ comment: value })}
                        />
                    </Neomorph>
                    <Neomorph
                        inner // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.addtxtnmop}>
                        <Input
                            placeholder=" Address"
                            numberOfLines={3}
                            multiline={true}
                            inputContainerStyle={signupStyles.tinput}
                            onChangeText={value => this.setState({ comment: value })}
                        />
                    </Neomorph>
                    <Neomorph
                        inner={this.state.eff} // <- enable shadow inside of neomorph
                        swapShadows // <- change zIndex of each shadow color
                        style={signupStyles.btnNopm}>
                        <Text style={{ paddingTop: 15, fontWeight: 'bold', color: '#C58E4C', }}>Register</Text>
                    </Neomorph>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Login') }}>
                        <Text style={{ top: 20, left: 150, fontWeight: 'bold' }}>Already Registered ?</Text>
                    </TouchableOpacity>

                </View>
                </ScrollView>
            </SafeAreaView>
           
        )
    }
}


