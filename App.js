import React from 'react';
import LoginScreen from './src/Screens/LoginScreen';
import WelcomeScreen from './src/Screens/WelcomeScreen';
import Signup from './src/Screens/SignupScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Dashboard from './src/Screens/Dashboard';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Neomorph } from 'react-native-neomorph-shadows';
import AddCompany from './src/Screens/AddCompany';
import History from './src/Screens/History';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Logout from './src/Screens/Logout';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const App = () => {

  function MyTabs() {
    return (
      <Tab.Navigator>
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              focused ?
                <Neomorph
                  swapShadows // <- change zIndex of each shadow color
                  style={{
                    shadowRadius: 10,
                    borderRadius: 25,
                    backgroundColor: 'white',
                    width: 50,
                    height: 50,
                    bottom: 10
                  }}
                >
                  <MaterialIcons name="dashboard" style={{ top: 13, left: 13 }} color='#feba24' size={25} />
                </Neomorph>
                : <MaterialIcons name="dashboard" color='#feba24' size={25} />
            ),
          }}
          name="Dashboard" component={Dashboard} />
        <Tab.Screen options={{
          headerShown: false,
          tabBarLabel: 'AddCompany',
          tabBarIcon: ({ focused }) => (
            focused ?
              <Neomorph
                swapShadows // <- change zIndex of each shadow color
                style={{
                  shadowRadius: 10,
                  borderRadius: 25,
                  backgroundColor: 'white',
                  width: 50,
                  height: 50,
                  bottom: 10
                }}
              >
                <MaterialIcons name="add-business" style={{ top: 13, left: 13 }} color='#feba24' size={25} />
              </Neomorph>
              : <MaterialIcons name="add-business" color='#feba24' size={25} />
          ),
        }} name="AddCompany" component={AddCompany} />
        <Tab.Screen
        
          options={{
            headerShown: false,
            tabBarLabel: 'History',
            tabBarIcon: ({ focused }) => (
              focused ?
                <Neomorph
                  swapShadows // <- change zIndex of each shadow color
                  style={{
                    shadowRadius: 10,
                    borderRadius: 25,
                    backgroundColor: 'white',
                    width: 50,
                    height: 50,
                    bottom: 10
                  }}
                >
                  <MaterialIcons name="history" style={{ top: 12, left: 12 }} color='#feba24' size={25} />
                </Neomorph>
                : <MaterialIcons name="history" color='#feba24' size={25} />
            ),
          }} name="History" component={History} />
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarLabel: 'LogOut',
            tabBarIcon: ({ focused }) => (
              focused ?
                <Neomorph
                  swapShadows // <- change zIndex of each shadow color
                  style={{
                    shadowRadius: 10,
                    borderRadius: 25,
                    backgroundColor: 'white',
                    width: 50,
                    height: 50,
                    bottom: 10
                  }}
                >
                  <MaterialIcons name="logout" style={{ top: 13, left: 13 }} color='#feba24' size={25} />
                </Neomorph>
                : <MaterialIcons name="logout" color='#feba24' size={25} />
            ),
          }} name="LogOut" component={Logout} />
      </Tab.Navigator>
    );
  }

  function LoginScreens() {
    return (
      <Stack.Navigator>
        <Stack.Screen options={{ headerShown: false }} name="Login" component={LoginScreen} />
        <Stack.Screen options={{ headerShown: false }} name="Signup" component={Signup} />
      </Stack.Navigator>
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Welcome">
        <Stack.Screen options={{ headerShown: false }} name="Welcome" component={WelcomeScreen} />
        <Stack.Screen options={{ headerShown: false }} name="LoginScreens" component={LoginScreens} />
        <Stack.Screen options={{ headerShown: false }} name="tab" component={MyTabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;
